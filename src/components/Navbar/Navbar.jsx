import { Link } from "react-router-dom";
import "bootstrap/dist/js/bootstrap.bundle.min.js";

const Navbar = ({ Toggle }) => {
   return (
      <nav className="navbar navbar-expand-lg bg-light text-dark">
         <div className="container-fluid">
            <Link to="/" className="navbar-brand d-none d-md-block">
               <strong>Dashboard Attendance</strong>
            </Link>
            <Link
               to="/"
               className="navbar-brand d-block d-md-none"
               onClick={Toggle}>
               <i className="bi bi-justify"></i>
            </Link>
            <button
               className="navbar-toggler"
               type="button"
               data-bs-toggle="collapse"
               data-bs-target="#navbarSupportedContent"
               aria-controls="navbarSupportedContent"
               aria-expanded="false"
               aria-label="Toggle navigation">
               <span className="navbar-toggler-icon"></span>
            </button>
            <div
               className="collapse navbar-collapse"
               id="navbarSupportedContent">
               <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                  <li className="nav-item d-flex justify-content-center align-items-center">
                     <button type="button" className="btn btn-secondary btn-sm">
                        --All Organization--
                     </button>
                  </li>
                  <li className="nav-item">
                     <Link
                        to="/sun"
                        className="nav-link text-dark"
                        aria-current="page"
                        href="#">
                        <i className="bi bi-sun-fill"></i>
                     </Link>
                  </li>
                  <div className="form-check form-switch d-flex justify-content-center align-items-center">
                     <input
                        className="form-check-input"
                        type="checkbox"
                        role="switch"
                        id="flexSwitchCheckDefault"
                     />
                  </div>
                  <li className="nav-item">
                     <Link
                        to="/moon"
                        className="nav-link text-dark"
                        aria-current="page"
                        href="#">
                        <i className="bi bi-moon-fill"></i>
                     </Link>
                  </li>
                  <li className="nav-item mx-2">
                     <Link
                        to="/logout"
                        className="nav-link text-dark"
                        aria-current="page"
                        href="#">
                        <i className="bi bi-box-arrow-right"></i>
                     </Link>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
   );
};

export default Navbar;
