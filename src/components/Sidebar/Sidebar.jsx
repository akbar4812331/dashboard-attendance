import { useState } from "react"
import { Link } from "react-router-dom"
import "../Sidebar/Sidebar.css"
import "bootstrap-icons/font/bootstrap-icons.css"

const Sidebar = () => {
   const [active, setActive] = useState(1)

   return (
      <div className="sidebar d-flex justify-content-between flex-column bg-maroon text-white py-3 ps-3 pe-5 vh-100">
         <div>
            <span className="p-3 text-decoration-none text-white">
               <i className="bi bi-code-slash fs-4 me-4"></i>
               <span className="fs-3">Cudo</span>
            </span>
            <hr className="text-white mt-2" />
            <ul className="nav nav-pills flex-column mt-3">
               <li
                  className={
                     active === 1 ? "active nav-item p-2" : "nav-item p-2"
                  }
                  onClick={() => setActive(1)}>
                  <Link to="/" className="p-1 text-decoration-none text-white">
                     <i className="bi bi-speedometer me-3 fs-5"></i>
                     <span className="fs-5">Dashboard</span>
                  </Link>
               </li>
               <li
                  className={
                     active === 2 ? "active nav-item p-2" : "nav-item p-2"
                  }
                  onClick={() => setActive(2)}>
                  <Link
                     to="/users"
                     className="p-1 text-decoration-none text-white">
                     <i className="bi bi-people me-3 fs-5"></i>
                     <span className="fs-5">Users</span>
                  </Link>
               </li>
               <li
                  className={
                     active === 3 ? "active nav-item p-2" : "nav-item p-2"
                  }
                  onClick={() => setActive(3)}>
                  <Link
                     to="/orders"
                     className="p-1 text-decoration-none text-white">
                     <i className="bi bi-table me-3 fs-5"></i>
                     <span className="fs-5">Orders</span>
                  </Link>
               </li>
               <li
                  className={
                     active === 4 ? "active nav-item p-2" : "nav-item p-2"
                  }
                  onClick={() => setActive(4)}>
                  <Link
                     to="/report"
                     className="p-1 text-decoration-none text-white">
                     <i className="bi bi-grid me-3 fs-5"></i>
                     <span className="fs-5">Report</span>
                  </Link>
               </li>
            </ul>
         </div>
         <div>
            <hr className="text-white" />
            <div className="nav-item p-2">
               {/* eslint-disable-next-line */}
               <a href="" className="p-1 text-decoration-none text-white">
                  <span className="fs-6">&copy; 2021 Cudo</span>
               </a>
            </div>
         </div>
      </div>
   )
}

export default Sidebar
