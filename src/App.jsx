import { useState, useEffect } from "react"
import { BrowserRouter, Routes, Route } from "react-router-dom"

import Sidebar from "./components/Sidebar/Sidebar"
import Navbar from "./components/Navbar/Navbar"

import Home from "./pages/Home"
import Users from "./pages/Users"
import Orders from "./pages/Orders"
import Report from "./pages/Report"

import "bootstrap/dist/css/bootstrap.min.css"

const App = () => {
   const [toggle, setToggle] = useState(false)
   const handleToggle = () => setToggle(!toggle)

   useEffect(() => {
      const handleSize = () => {
         if (window.innerWidth > 768) {
            setToggle(false)
         }
      }

      window.addEventListener("resize", handleSize)

      return () => {
         window.removeEventListener("resize", handleSize)
      }
   }, [])

   return (
      <BrowserRouter>
         <div className="d-flex" style={{ backgroundColor: "#F3F3F3" }}>
            <div className={toggle ? "d-none" : "w-auto position-fixed"}>
               <Sidebar />
            </div>
            <div className={toggle ? "d-none" : "invisible"}>
               <Sidebar />
            </div>
            <div className="col overflow-auto">
               <Navbar Toggle={handleToggle} />
               <Routes>
                  <Route path="/" element={<Home />}></Route>
                  <Route path="/users" element={<Users />}></Route>
                  <Route path="/orders" element={<Orders />}></Route>
                  <Route path="/report" element={<Report />}></Route>
               </Routes>
            </div>
         </div>
      </BrowserRouter>
   )
}

export default App
