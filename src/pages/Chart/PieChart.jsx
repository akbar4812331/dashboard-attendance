import { useState, useEffect } from "react"
import ApexChart from "react-apexcharts"

const PieChart = () => {
   const [series, setSeries] = useState([])
   const [series2, setSeries2] = useState([])
   const [options, setOptions] = useState({})
   const [attendance, setAttendance] = useState({})

   useEffect(() => {
      const getData = async () => {
         const url = "http://localhost:5000/donut"
         try {
            const response = await fetch(url)
            const data = await response.json()
            console.log(data)
            console.log(data?.attendance)
            setSeries(data?.series)
            setSeries2(data?.series2)
            setOptions(data?.options)
            setAttendance(data?.attendance)
         } catch (error) {
            console.log(error)
         }
      }
      getData()
   }, [])

   return (
      <>
         <div className="row">
            <div className="col-6">
               <div className="card">
                  <p className="py-3 px-3">
                     <strong className="text-danger">Today </strong>
                     On Duty
                  </p>
                  <ApexChart options={options} series={series} type="donut" />
               </div>
            </div>
            <div className="col-6">
               <div className="card">
                  <p className="py-3 px-3">
                     <strong className="text-danger">Today </strong>
                     Attendance
                  </p>
                  <ApexChart options={options} series={series2} type="donut" />
               </div>
            </div>
         </div>

         <div className="row mt-4">
            <div className="col-6">
               <div className="card">
                  <p className="py-3 px-3">
                     <strong className="text-danger">Today </strong>
                     Feed / Activity
                  </p>
                  <div className="mb-3 text-center">
                     <h1>{attendance?.feed?.person?.length}</h1>
                     <small className="font-weight-bold">
                        OF
                        <span style={{ color: "green", fontWeight: "bold" }}>
                           {attendance?.feed?.person?.length}
                        </span>
                        PERSON
                     </small>
                  </div>
               </div>
            </div>

            <div className="col-6">
               <div className="card">
                  <p className="py-3 px-3">
                     <strong className="text-danger">Today </strong>
                     Patroli
                  </p>
                  <div className="mb-3 text-center">
                     <h1>{attendance?.patrol?.person?.length}</h1>
                     <small className="font-weight-bold">Person</small>
                  </div>
               </div>
            </div>
         </div>

         <div className="row mt-4">
            <div className="col-6">
               <div className="card">
                  <p className="py-3 px-3">
                     <strong className="text-danger">Today </strong>
                     Non Schedule
                  </p>
                  <div className="mb-3 text-center">
                     <h1>{attendance?.schedule?.person?.length}</h1>
                     <small className="font-weight-bold">Person</small>
                  </div>
               </div>
            </div>

            <div className="col-6">
               <div className="card">
                  <p className="py-3 px-3">
                     <strong className="text-danger">Today </strong>
                     Non Geofence
                  </p>
                  <div className="mb-3 text-center">
                     <h1>{attendance?.geofence?.person?.length}</h1>
                     <small className="font-weight-bold">Person</small>
                  </div>
               </div>
            </div>
         </div>
      </>
   )
}

export default PieChart
