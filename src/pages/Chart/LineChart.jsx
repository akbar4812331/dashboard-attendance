import { useState, useEffect } from "react"
import ApexChart from "react-apexcharts"

const LineChart = () => {
   const [series, setSeries] = useState([])
   const [series2, setSeries2] = useState([])
   const [options, setOptions] = useState({})
   const [options2, setOptions2] = useState({})

   useEffect(() => {
      const getData = async () => {
         const url = "http://localhost:5000/line"
         try {
            const response = await fetch(url)
            const data = await response.json()
            console.log(data)
            setSeries(data?.series)
            setOptions(data?.options)
         } catch (error) {
            console.log(error)
         }
      }
      getData()
   }, [])

   useEffect(() => {
      const getData2 = async () => {
         const url = "http://localhost:5000/lineTwo"
         try {
            const response = await fetch(url)
            const data = await response.json()
            console.log(data)
            setSeries2(data?.series2)
            setOptions2(data?.options)
         } catch (error) {
            console.log(error)
         }
      }
      getData2()
   }, [])

   return (
      <>
         <div className="row">
            <div className="col-12">
               <div className="card">
                  <ApexChart
                     options={options}
                     series={series}
                     type="line"
                     height={350}
                  />
               </div>
            </div>
         </div>

         <div className="row mt-3">
            <div className="col-12">
               <div className="card">
                  <ApexChart
                     options={options2}
                     series={series2}
                     type="line"
                     height={350}
                  />
               </div>
            </div>
         </div>
      </>
   )
}

export default LineChart
