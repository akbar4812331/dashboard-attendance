import LineChart from "./Chart/LineChart"
import PieChart from "./Chart/PieChart"

const Home = () => {
   return (
      <div className="p-3">
         <div className="container-fluid w-auto">
            <div className="row">
               <div className="col-12 col-md-6 p-2">
                  <PieChart />
               </div>
               <div className="col-12 col-md-6 p-2">
                  <LineChart />
               </div>
            </div>
         </div>
      </div>
   )
}

export default Home
