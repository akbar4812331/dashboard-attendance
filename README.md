# Dashboard Attendance

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload
$ yarn dev

# run fake api using json-server
$ npx json-server --watch db.json --port=5000

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```
